package com.crucru.searchandplay.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crucru.searchandplay.R;
import com.crucru.searchandplay.model.MainListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-09.
 */
public class MainListViewAdapter extends BaseAdapter  {

    Context context;
    private LayoutInflater inflater;
    private ArrayList<MainListModel> listArr;


    public MainListViewAdapter(Context context, LayoutInflater inflater, ArrayList<MainListModel> listArr) {
        this.context = context;
        this.inflater = inflater;
        this.listArr = listArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.mainlistviewitem, null);
        if(convertView != null){

            ImageView imageView = (ImageView)convertView.findViewById(R.id.main_listitem_img);
            TextView title = (TextView)convertView.findViewById(R.id.main_listitem_title);

            MainListModel data = listArr.get(position);

            Picasso.with(context).load(data.getImgUrl()).into(imageView);
            title.setText(data.getTitle());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
