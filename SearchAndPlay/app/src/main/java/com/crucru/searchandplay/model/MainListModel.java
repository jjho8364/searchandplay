package com.crucru.searchandplay.model;

/**
 * Created by Administrator on 2016-07-09.
 */
public class MainListModel {

    private String imgUrl;
    private String title;
    private String linkUrl;

    public MainListModel(){

    }

    public MainListModel(String imgUrl, String title, String linkUrl) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.linkUrl = linkUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }
}
