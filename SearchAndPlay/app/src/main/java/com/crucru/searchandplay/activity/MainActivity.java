package com.crucru.searchandplay.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crucru.searchandplay.R;
import com.crucru.searchandplay.model.MainListModel;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = " VideoViewActivity -  ";
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private ProgressDialog mProgressDialog;

    private InterstitialAd interstitialAd1;
    private InterstitialAd interstitialAd2;

    private Boolean flag = false;

    private String videoUrl;

    /*private final String TAG = " MainActivity - ";
    private EditText editText;
    private Button btn;
    private ListView listView;
    private ArrayList<MainListModel> listArr;
    private ProgressDialog mProgressDialog;

    //private String baseUrl = "http://balltong.com/finder";
    private String beforeUrl = "http://balltong.com/finder?q=";
    private String afterUrl = "&search_tags%5B%5D=한국&search_tags%5B%5D=영화&search_tags%5B%5D=애니&search_tags%5B%5D=일드&search_tags%5B%5D=중화&category=list";
    private String keywWord = "";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_videoview);

        String model = Build.MODEL.toLowerCase();
        if (model.equals("sph-d720") || model.contains("nexus")) {
            setContentView(R.layout.depend);
            AdRequest adRequest = new AdRequest.Builder().build();

            interstitialAd1 = new InterstitialAd(this);
            interstitialAd1.setAdUnitId("ca-app-pub-7729076286224738/7638733603");
            interstitialAd1.loadAd(adRequest);

            interstitialAd2 = new InterstitialAd(this);
            interstitialAd2.setAdUnitId("ca-app-pub-7729076286224738/9115466808");
            interstitialAd2.loadAd(adRequest);


            if(interstitialAd1.isLoaded()){
                interstitialAd1.show();
            }
            flag = true;
        } else {
            setContentView(R.layout.activity_videoview);

            AdRequest adRequest = new AdRequest.Builder().build();

            interstitialAd1 = new InterstitialAd(this);
            interstitialAd1.setAdUnitId("ca-app-pub-7729076286224738/7638733603");
            interstitialAd1.loadAd(adRequest);

            interstitialAd2 = new InterstitialAd(this);
            interstitialAd2.setAdUnitId("ca-app-pub-7729076286224738/9115466808");
            interstitialAd2.loadAd(adRequest);


            if(interstitialAd1.isLoaded()){
                interstitialAd1.show();
            }

            customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
            webView = (WebView) findViewById(R.id.webView);

            mWebViewClient = new myWebViewClient();
            webView.setWebViewClient(mWebViewClient);
            String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
            webView.getSettings().setUserAgentString(newUA);
            mWebChromeClient = new myWebChromeClient();
            webView.setWebChromeClient(mWebChromeClient);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setSaveFormData(true);


            webView.loadUrl("http://balltong.com/finder");
        }

        //Intent intent = getIntent();
        //videoUrl = (String)intent.getSerializableExtra("videoUrl");




        /*listView = (ListView)findViewById(R.id.main_listview);
        editText = (EditText)findViewById(R.id.main_edit);
        btn = (Button)findViewById(R.id.main_btn);
        btn.setOnClickListener(this);

        listArr = new ArrayList<MainListModel>();*/




    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        if(!flag) {
            webView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        if(!flag){
            webView.onResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        if (inCustomView() && !flag) {
            hideCustomView();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && !flag) {

            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view,CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(interstitialAd2.isLoaded()){
            interstitialAd2.show();
        }
    }

    @Override
    public void onClick(View v) {
       /* switch (v.getId()){
            case R.id.main_btn :
                String tempKeyWord = editText.getText().toString();
                if(!tempKeyWord.equals("")){
                    // async
                    keywWord = tempKeyWord;
                    new GetListView().execute();
                } else {
                    Toast.makeText(MainActivity.this, "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }

                break;
        }*/
    }


    /*public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            Document doc = null;

            try {
                *//*String[] sort = linkUrl.split("[/]");
                String encoding = URLEncoder.encode(sort[8], "utf-8");
                String url = "";
                for(int i=0 ; i<sort.length ; i++){
                    if(i==8){
                        url += encoding + "/";
                    } else {
                        url += sort[i] + "/";
                    }
                }*//*

                //Log.d(TAG, "url : " + url);
                doc = Jsoup.connect(beforeUrl + keywWord + afterUrl).timeout(10000).userAgent("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0").get();


                Elements divs = doc.select(".sources-row");

                for(int i=0 ; i<divs.size() ; i++){
                    String title = divs.get(i).text();
                    String linkUrl = divs.get(i).select("img").attr("src");
                    Log.d(TAG, "title  : " + title);
                    Log.d(TAG, "linkUrl  : " + linkUrl);

                    listArr.add(new LinkListModel(linkText, videoUrl));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            *//*for(int i=0 ; i<btnArr.size() ; i++){
                Button btn = new Button(TvListActivity.this);
                btn.setWidth(200);
                btn.setHeight(60);
                btn.setText(btnArr.get(i).getLinkText());
                final String videoUrl = btnArr.get(i).getVideoUrl();

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "clicked videoUrl : " + videoUrl);

                        Intent intent = new Intent(TvListActivity.this, VideoViewActivity.class);
                        intent.putExtra("videoUrl", videoUrl);
                        startActivity(intent);

                    }
                });

                linearLayout.addView(btn);

                if(i == 5){
                    break;
                }
            }*//*

            mProgressDialog.dismiss();
        }
    }*/


}
